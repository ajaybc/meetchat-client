$(document).ready(function(){
	Methods.initialize();
});


var Globalvars = {
	"nodeServer" : "http://ajaybc-chatserver.nodejitsu.com",
	"socket" : "", //Socket io variable
	"users" : [], //holds the list of online users,
	"me" : null,
	"pc" : null,
	"iceCandidateArray" : []
}

//All methods / functions go here
var Methods = {
	"initialize" : function(){
		//Startup script.
		Globalvars.socket = io.connect(Globalvars.nodeServer);
		Methods.compileTemplates();
		Methods.polyFill();
		/*$.ajaxSetup({ cache: true });
		$.getScript('//connect.facebook.net/en_UK/all.js', function(){
			FB.init({
				appId: '290818514318209',
				status: true,
			});

			FB.getLoginStatus(Methods.updateFBLoginStatusCallback);
		});*/

		Methods.updateFBLoginStatusCallback({});


		Globalvars.socket.on('call', function (signal) {
			if(!Globalvars.pc){
		        Methods.startCall(false, signal);
			}

			if(signal.sdp){
				temp = new RTCSessionDescription({"sdp" : decodeURIComponent(signal.sdp), "type" : signal.type});
		    	Globalvars.pc.setRemoteDescription(temp);
		    	for(i = 0; i < Globalvars.iceCandidateArray.length; i++){
		    		Globalvars.pc.addIceCandidate(new RTCIceCandidate({
					    sdpMLineIndex: decodeURIComponent(signal.sdpMLineIndex),
					    candidate: decodeURIComponent(signal.candidate)
					}));
		    	}

		    	Globalvars.iceCandidateArray = [];
			}
			else{
				if(Globalvars.pc.remoteDescription){
					Globalvars.pc.addIceCandidate(new RTCIceCandidate({
					    sdpMLineIndex: decodeURIComponent(signal.sdpMLineIndex),
					    candidate: decodeURIComponent(signal.candidate)
					}));
					console.log("remot");
				}
				else{
					Globalvars.iceCandidateArray.push(new RTCIceCandidate({
					    sdpMLineIndex: decodeURIComponent(signal.sdpMLineIndex),
					    candidate: decodeURIComponent(signal.candidate)
					}));
					console.log("ice candidate to temp array");
				}
			}
		});


		$("#roster-wrap").on("click", ".roster-list-item", function(e){
			//Globalvars.socket.emit('call', {"receiver_id" : $(this).attr("data-id"), "caller_id" : Globalvars.me.id});
			params = {"receiver_id" : $(this).attr("data-id"), "caller_id" : Globalvars.me.id};
			Methods.startCall(true, params);
			e.preventDefault();
		});
	},

	// run start(true) to initiate a call
	"startCall" : function (isCaller, params) {
		var configuration = {"iceServers": [{"url": "stun:stun.l.google.com:19302"}]};
		Globalvars.pc = new RTCPeerConnection(configuration);

		// send any ice candidates to the other peer
		Globalvars.pc.onicecandidate = function (evt) {
			//alert("ice candidate");
			if (!Globalvars.pc || !evt || !evt.candidate) return;
			var candidate = evt.candidate;
			Globalvars.socket.emit("call",{ "candidate": encodeURIComponent(candidate.candidate), "sdpMLineIndex" : encodeURIComponent(candidate.sdpMLineIndex), "receiver_id" :  params.receiver_id, "caller_id" : params.caller_id});
		};

		// once remote stream arrives, show it in the remote video element
		Globalvars.pc.onaddstream = function (evt) {
			console.log("add stream");
			if (!event) return;
			$("#remote-video").attr("src",URL.createObjectURL(evt.stream));
			Methods.waitUntilRemoteStreamStartsFlowing();
		};

		// get the local stream, show it in the local video element and send it
		navigator.getUserMedia({ "audio": false, "video": true }, function (stream) {
			$("#my-video").attr("src", URL.createObjectURL(stream));
			Globalvars.pc.addStream(stream);

			if (isCaller){
				Globalvars.pc.createOffer(getDescription, null, { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } });
			}
			else{
				console.log("Got Remote Description");
				console.log(Globalvars.pc.remoteDescription);				
				//Globalvars.pc.createAnswer(Globalvars.pc.remoteDescription, getDescription);
				Globalvars.pc.createAnswer(getDescription, null, { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } });
			}

			function getDescription(desc) {
				Globalvars.pc.setLocalDescription(desc);
				console.log("my desc");
				console.log(desc);
				Globalvars.socket.emit("call", {"sdp": encodeURIComponent(desc.sdp), "type": desc.type, "receiver_id" :  params.receiver_id, "caller_id" : params.caller_id});
				//signalingChannel.send(JSON.stringify({ "sdp": desc }));
			}
		});
	},

	"updateFBLoginStatusCallback" : function(response){
		/*console.log(response);
		if(response.status == "connected"){
			FB.api('/me', function(response) {
				Methods.afterLogin(response);
				//Methods.fetchOnlineUsers(true);
			});
		}
		else{
			$("#loginwithfb-wrap").show();
			$("#loginwithfb-btn").on("click", function(e){
				FB.login(function(response) {
					if (response.authResponse) {
						console.log('Welcome!  Fetching your information.... ');
						FB.api('/me', function(response) {
							Methods.afterLogin(response);
						});
					}
					else {
						console.log('User cancelled login or did not fully authorize.');
					}
				});

				e.preventDefault();
			})
		}*/

		response = new Object;
		response.first_name = "firstname_" + Math.floor((Math.random() * 10000));
		response.last_name = "lastname_" + Math.floor((Math.random() * 10000));
		response.username = "username_" + Math.floor((Math.random() * 10000));
		response.id = Math.floor((Math.random() * 10000));
		Methods.afterLogin(response);
	},


	"afterLogin" : function(response){
		console.log(response);
		Globalvars.socket.emit('login', response);
		Globalvars.me = response;
		$("#loginwithfb-wrap").hide();
		$("#roster-wrap").show();

		Globalvars.socket.on('userchannel', function(users){
			console.log("user channel");
			console.log(users);
			Globalvars.users = [];
			for(i = 0; i < users.length; i++){
				if(users[i].id != Globalvars.me.id){
					//insert all users except the current user into the users list
					users[i].initials = users[i].first_name.substring(0, 1) + users[i].last_name.substring(0, 1);
					Globalvars.users.push(users[i]);
				}
				else{
					console.log(Globalvars.me.id);
				}
			}

			Methods.renderRosterList();
		});
	},



	"renderRosterList" : function(){
		html = "";

		if(Globalvars.users.length == 0){
			html += "<li>No users online right now.</li>"
		}
		else{
			for(i = 0; i < Globalvars.users.length; i++){
				html += Templates.rosterListItem.render(Globalvars.users[i], Templates.rosterListItem);
			}
		}

		$("#roster-list").html(html);
	},


	"fetchOnlineUsers" : function(render){
		$.ajax({
			"url" : Globalvars.nodeServer + "/users",
			"success" : function(data){
				data = JSON.parse(data);
				for(i = 0; i < data.length; i++){
					if(data[i].id != Globalvars.me.id){
						//insert all users except the current user into the users list
						data[i].initials = data[i].first_name.substring(0, 1) + data[i].last_name.substring(0, 1);
						Globalvars.users.push(data[i]);
					}
				}

				if(render){
					Methods.renderRosterList();
				}
			}
		});
	},


	"compileTemplates" : function(){
		Templates.rosterListItem = Hogan.compile($("#roster-list-item-template").html());
	},

	"getUserFromId" : function(id){
		for(i = 0; i < Globalvars.users.length; i++){
			if(Globalvars.users[i].id == id){
				return Globalvars.users[i];
			}
		}
	},

	"polyFill" : function(){
		//Do cross browser compatibility fixes
		window.RTCPeerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
		window.RTCSessionDescription = window.RTCSessionDescription || window.webkitRTCSessionDescription || window.mozRTCSessionDescription;
		window.RTCIceCandidate = window.RTCIceCandidate || window.webkitRTCIceCandidate || window.mozRTCIceCandidate;
		navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
	},

	"waitUntilRemoteStreamStartsFlowing" : function(){
		var remote_video = $("#remote-video").get(0);
		if (!(remote_video.readyState <= HTMLMediaElement.HAVE_CURRENT_DATA 
        || remote_video.paused || remote_video.currentTime <= 0)) 
	    {
	        // remote stream started flowing!
	    } 
	    else setTimeout(Methods.waitUntilRemoteStreamStartsFlowing, 50);
	}
}



var Templates = {
	"rosterListItem" : null,
}